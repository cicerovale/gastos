<?php
require __DIR__ . '/vendor/autoload.php';

setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
define('HOME', 'http://localhost:3030');
define('VIEWS_PATH', __DIR__ . '/views/');
define('DB_NAME','my_expenses');
define('DB_HOST','127.0.0.1');
define('DB_USER','root');
define('DB_PASSWORD','');
define('DB_CHARSET','UTF8');