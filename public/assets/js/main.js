const qs = (el) => document.querySelector(el);
const qsa = (el) => document.querySelectorAll(el);
//   loader ------------------
$(document).ready(function () {
    $('.loader-wrap').fadeOut(1000, function () {
        $("#main").animate({
            opacity: '1',
        }, 1000);
    });

    $('.navToggle').click(function (event) {
        event.preventDefault();
        $(this).toggleClass('active');
        $('body').toggleClass('locked');
        $('.menuContent').toggleClass('open');
        $('.logoHeader').toggleClass('disabled');
    })

    $('.formControl').attr('autocomplete', 'off');

    $('.viewerPassword').on('click', function (event) {
        event.preventDefault();
        var passwordField = $('#password');
        var passwordFieldType = passwordField.attr('type');
        if (passwordFieldType == 'password') {
            passwordField.attr('type', 'text');
            $(this).val('hide');
        } else {
            passwordField.attr('type', 'password');
            $(this).val('show');
        }
    });

    // ModalConfirm
    $('a[data-confirm]').click(function (event) {
        event.preventDefault();
        var href = $(this).attr('href');

        $('#DeletedConfirm').attr('href', href);
        $('#confirmDelete').toggleClass('active');
    });

    $('.modal .closed').click(function (event) {
        $('#confirmDelete').removeClass('active');
    });
    now = new Date;
    let year = now.getFullYear();
    console.log(year);
    $.datepicker.setDefaults({
        dateFormat: 'dd/mm/yy',
        dayNamesMin: ["Dom" , "Seg" , "Ter", "Qua" , "Qui" , "Sex" , "Sab"],
        dayNames: ["Domingo" , "Segunda" , "Terça", "Quarta" , "Quinta" , "Sexta" , "Sábado"],
        monthNames: ["Janeiro" , "Fevereiro" , "Março", "Abril" , "Maio" , "Junho" , "Julho" , "Agosto" , "Setembro" , "Outubro", "Novembro" , "Dezembro"],
        monthNamesShort: ["Jan" , "Fev" , "Mar", "Abr" , "Maio" , "Jun" , "Jul" , "Ago" , "Set" , "Out", "Nov" , "Dez"],
        buttonImage: 'assets/images/logo.png',
        closeText: 'Fechar',
        prevText: 'Anterior',
        nextText: 'Próximo',
        yearRange: '2019:2020'
    });


})
