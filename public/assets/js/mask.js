var mask = (function() {

    var obj = {};

    obj.mascara = function(){

        $('.cpfMask').mask('000.000.000-00', {reverse: true});
        $('.cnpjMask').mask('00.000.000/0000-00', {reverse: true});
        $('.cardMask').mask('0000 0000 0000 0000', {reverse: true});
        $('.dateMask').mask('00/00/0000');
        $('.cvvMask').mask('000');
        $('.moneyMask').mask('000.000.000.000.000.00', {reverse: true});
        $('.zipcodeMask').mask('00.000-000');
        $('.ufMask').mask('AA');

        var SPMaskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            spOptions = {
                onKeyPress: function(val, e, field, options) {
                    field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };
        $('.phoneMask').mask(SPMaskBehavior, spOptions);
    };


    return {
        init: function(){
            obj.mascara();

        }
    }

})();

$(function(){
    mask.init();
});