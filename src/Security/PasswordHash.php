<?php


namespace Code\Security;


class PasswordHash
{
    public function hash($string)
    {
        return password_hash($string, PASSWORD_ARGON2I);
    }
    public function check($string, $passwordHashed)
    {
        return password_verify($string, $passwordHashed);
    }
}