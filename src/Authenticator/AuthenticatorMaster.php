<?php
namespace Code\AuthenticatorMaster;

use Code\Entity\UserAdmin;
use Code\Session\Session;

class AuthenticatorMaster
{
	/**
	 * @var UserAdmin
	 */
	private $user;

	public function __construct(UserAdmin $user = null)
	{
		$this->user = $user;
	}

	public function login(array $credentials)
	{
		$user = current($this->user->where([
			'user' => $credentials['user'],
		]));

		if(!$user){
			return false;
		}
		if($user['password'] != $credentials['password']) {
			return false;
		}

		unset($user['password']);
		Session::add('user', $user);
		return true;
	}

	public function logout()
	{
		if(Session::has('user')) {
			Session::remove('user');
		}
		Session::clear();
	}
}